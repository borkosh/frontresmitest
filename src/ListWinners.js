import React,{useState} from 'react'
import './App.css';
import {Header,Table } from 'semantic-ui-react'

// export interface Divination{
//     Name : string;
//     PhoneNumber: string;
//     Degree : number;
// }

// export interface IProps{
//     listWinners: Divination[]
// }


const ListWinners = (props) => {
    const [divinations, setDivinations] = useState(props.listWinners);
    console.log("divinations= ",divinations);
    
    return (
        <>
        <Table celled  fixed>     
      <Table.Header>   
        <Table.Row>
    <Table.HeaderCell >ID</Table.HeaderCell>
          <Table.HeaderCell>Фамилия</Table.HeaderCell>
          <Table.HeaderCell>Номер</Table.HeaderCell>
          <Table.HeaderCell>Градус</Table.HeaderCell>
         
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {  divinations.map((divi,i) =>{
            return <Table.Row key={i} >
            <Table.Cell>{i}</Table.Cell>
            <Table.Cell>{divi.name}</Table.Cell>
            <Table.Cell>{divi.phoneNumber}</Table.Cell>
            <Table.Cell>{divi.degree}</Table.Cell>
            </Table.Row>  
                  }) }
       </Table.Body>     
    </Table>
        </>
    )

}
export default ListWinners;