import React,{useState} from 'react';
import './App.css';
import ErrorList from './ErrorList'
import axios from 'axios';
import { Form,Button,Header } from 'semantic-ui-react'
import { Redirect } from "react-router-dom";

export interface Divination{
    Name : string;
    PhoneNumber: string;
    Degree : number;
}

const AddDivination = () => {
    const [divination, setDivination] = useState<Divination>({
        Name: '',
        PhoneNumber: '',
        Degree: 0,
    });
    const [errList,setErrList]=useState<string[]>([])
    const [redirect,setRedirect]=useState(false)

    const putDivination=()=>{

        const config = {headers: {'Access-Control-Allow-Origin': '*'}};
        const requestDivination=divination;
        try {
            axios.post(`http://localhost:5000/Divination/new`, requestDivination,config)
              .then(res => {
                console.log("res= ", res);   
                setRedirect(true);
              }).catch(error => {
                let errListNew: Array<string>=[];
                console.log("1error.response = ",error.response)
                console.log("1errList = ",errList)
                if (error.response.data.Name){
                    errListNew.push(error.response.data.Name[0]);
                }
                if (error.response.data.PhoneNumber){
                    errListNew.push(error.response.data.PhoneNumber[0]);
                }
                if (error.response.data.Degree){
                    errListNew.push(error.response.data.Degree[0]);
                }
               //if(errListNew.length>0)
                    setErrList(errListNew);
                
                console.log("2error.response = ",error.response)
                console.log("2errList = ",errList)
            });
            } catch (e) {
              console.log(`😱 Axios request failed: ${e}`);
            }
    } 

    const onChangeName=(e:React.ChangeEvent<HTMLInputElement>) =>{
        console.log("divination ",divination);
        divination.Name=e.target.value
        setDivination(divination)        
    }
    const onChangePhone=(e:React.ChangeEvent<HTMLInputElement>) =>{
        console.log("divination ",divination);
        divination.PhoneNumber=e.target.value
        setDivination(divination)        
    }
    const onChangeDegree=(e:React.ChangeEvent<HTMLInputElement>) =>{
        console.log("divination ",divination);
        divination.Degree=Number(e.target.value)
        setDivination(divination)        
    }

    const onClickSend=(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) =>{
        console.log("divination ",divination);

        putDivination()        
    }

    if (redirect) {
        return <Redirect to = '/list' />;
      }
     
    
  return (
      <div id="addform">
          <Header as='h4'>Какая погода будет завтра?</Header>
    <Form>
    <Form.Field>
      <label>Имя</label>
      <input placeholder='Имя' name="Name" onChange={(e:any)=>onChangeName(e)} />
    </Form.Field>
    <Form.Field>
      <label>Номер телефона</label>
      <input placeholder='77775554433' name="PhoneNumber" onChange={(e:any)=>onChangePhone(e)} />
    </Form.Field>
    <Form.Field>
      <label>Кол-во градусов Цельсия</label>
      <input  type='number' min={-100} max={100} placeholder='Кол-во градусов Цельсия'  name="Degree" onChange={(e:any)=>onChangeDegree(e)}/>
    </Form.Field>
    <Form.Field>
    </Form.Field>
    <Button type='submit' onClick={onClickSend}>Отправить предсказание</Button>
  </Form>
  { errList? <ErrorList errList={errList}/>:''}
  
  </div>
      );
}
export default AddDivination;
