import React from 'react'
import './App.css';
import {Header } from 'semantic-ui-react'


const ErrorList = (props: { errList: string[] }) => {
    return (
        <>
        {  props.errList.map(err =>{
            return     <Header as='h5' className="err">{err}</Header>  
                  }) }
        </>
    )

}
export default ErrorList;