import React from 'react';
import './App.css';
import Header from './Header';
import AddDivination from './AddDivination';
import ListDegree from './ListDegree';
import { Route, BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className='app-wrapper'>
      <Header/>     
      <div className='app-wrapper-content'>
          <Route path="/addDivination" component={AddDivination}/>
          <Route path="/list" component={ListDegree}/>
      </div>
    </div>
    </BrowserRouter>
  );
}

export default App;
