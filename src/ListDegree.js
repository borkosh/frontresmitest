import React,{useState,useLayoutEffect} from 'react';
import './App.css';
import ListWinners from './ListWinners'
import axios from 'axios';
import { Form,Button,Header } from 'semantic-ui-react'
import { Redirect } from "react-router-dom";

// export interface Divination{
//     Name : string
//     PhoneNumber: string
//     Degree : number
// }

const ListDegree = () => {
    const [divinations, setDivinations] = useState();
    const [forecast, setForecast] = useState();

    const getDivination=()=>{

        const config = {headers: {'Access-Control-Allow-Origin': '*'}};
        const model ={a:1}
        try {
            axios.post(`http://localhost:5000/Divination/list`,model, config)
              .then(res => {
                console.log("res.data= ", res.data.data);   
                const persons = res.data.data;
                setDivinations(persons)
                console.log("divinations axios= ", divinations);   
              }).catch(error => {               
                console.log("error.response = ",error.response)               
            });
            } catch (e) {
              console.log(`Request failed: ${e}`);
            }
    } 

    const getForecast=()=>{

      const config = {headers: {'Access-Control-Allow-Origin': '*'}};
      try {
          axios.get(`http://localhost:5000/Divination/forecast`, config)
            .then(res => {
              console.log(" setForecast res.data= ", res.data.data);   
              setForecast(res.data.data)
              console.log("setForecast axios= ", forecast);   
            }).catch(error => {               
              console.log("error.response = ",error.response)               
          });
          } catch (e) {
            console.log(`Request failed: ${e}`);
          }
  } 


    useLayoutEffect(() => {
        getDivination();
        getForecast();
    }, []);  
    
  return (
      <div id="addform">
          <Header as='h4'>Результаты предсказаний в городе  { forecast? forecast.city+"на "+forecast.date+" ("+forecast.tempAvg+" градусов)" :""}</Header>
    
  { divinations? <ListWinners listWinners={divinations}/>:''}
  
  </div>
      );
}
export default ListDegree;
