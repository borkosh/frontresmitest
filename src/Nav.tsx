import React, { useState }  from 'react'
import { Menu } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom';

const Nav = () =>  {
    const [activeItem, setActiveItem] = useState("addDivination");

      return (
        <Menu stackable>
          <Menu.Item>
            <img alt="Logo" src='/logo192.png' />
          </Menu.Item>
          <Menu.Item
            name='addDivination'
            as={NavLink}       
            to='/addDivination'     
            active={activeItem === 'addDivination'}
            onClick={()=>setActiveItem('addDivination')}
          >
        Добавить предсказание
          </Menu.Item>
  


          <Menu.Item
            name='list'
            as={NavLink}       
            to='/list' 
            active={activeItem === 'list'}
            onClick={()=>setActiveItem('list')}
          >
            Список
          </Menu.Item>
  
        </Menu>
      )
  }
  export default Nav;