import React from 'react'
import Nav from './Nav'

const Header = () => {
    return (
        <header className="head">
            <Nav/> 
        </header>
    )

}
export default Header;